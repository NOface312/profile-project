#!/bin/sh

ssh -o StrictHostKeyChecking=no root@$VDS_SELECTEL_IP_SERVER << 'ENDSSH'
    cd /home
    export $(cat .env | xargs)
    docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    docker pull $IMAGE:django_backend
    docker pull $IMAGE:nginx
    docker-compose -f docker-compose.prod.yaml up -d --build
ENDSSH
