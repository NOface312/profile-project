from django.contrib.auth import authenticate, login
from django.views import View
from django.shortcuts import render, redirect
from django.views.generic import UpdateView
from users.forms import UserCreationForm, UserUpdateForm

from django.contrib.auth import get_user_model
User = get_user_model()


class Register(View):
    template_name = 'registration/register.html'

    def get(self, request):
        context = {
            'form': UserCreationForm()
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('home')
        context = {
            'form': form
        }
        return render(request, self.template_name, context)


class CreateApplication(View):
    template_name = 'home.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        return render(request, self.template_name)


class UserProfile(View):
    template_name = 'profile.html'

    def get(self, request):
        form = UserUpdateForm(instance=request.user)

        context = {
            'form': form
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = UserUpdateForm(data=request.POST, instance=request.user)
        if form.is_valid():
            form.save()

        context = {
            'form': form
        }
        return render(request, self.template_name, context)
