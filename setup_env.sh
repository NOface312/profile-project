#!/bin/sh

echo DEBUG=$DEBUG >> .env
echo SQL_ENGINE=$SQL_ENGINE >> .env
echo DATABASE=$DATABASE >> .env

echo SECRET_KEY=$SECRET_KEY >> .env
echo SQL_NAME=$SQL_NAME >> .env
echo SQL_USER=$SQL_USER >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env
echo SQL_HOST=$SQL_HOST >> .env
echo SQL_HOSTNAME=$SQL_HOSTNAME >> .env
echo SQL_PORT=$SQL_PORT >> .env

echo POSTGRES_DB=$POSTGRES_DB >> .env
echo POSTGRES_PASSWORD=$POSTGRES_PASSWORD >> .env
echo POSTGRES_USER=$POSTGRES_USER >> .env

echo DJANGO_IMAGE=$IMAGE:django_backend >> .env
echo NGINX_IMAGE=$IMAGE:nginx >> .env

echo VDS_SELECTEL_IP_SERVER=$VDS_SELECTEL_IP_SERVER >> .env
